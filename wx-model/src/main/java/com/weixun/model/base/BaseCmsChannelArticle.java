package com.weixun.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseCmsChannelArticle<M extends BaseCmsChannelArticle<M>> extends Model<M> implements IBean {

	public M setChannelarticlePk(java.lang.Integer channelarticlePk) {
		set("channelarticle_pk", channelarticlePk);
		return (M)this;
	}

	public java.lang.Integer getChannelarticlePk() {
		return getInt("channelarticle_pk");
	}

	public M setFkChannelPk(java.lang.String fkChannelPk) {
		set("fk_channel_pk", fkChannelPk);
		return (M)this;
	}

	public java.lang.String getFkChannelPk() {
		return getStr("fk_channel_pk");
	}

	public M setFkArticlePk(java.lang.String fkArticlePk) {
		set("fk_article_pk", fkArticlePk);
		return (M)this;
	}

	public java.lang.String getFkArticlePk() {
		return getStr("fk_article_pk");
	}

}
